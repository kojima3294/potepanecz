RSpec.feature "Products", type: :feature do
  let(:taxonomy) { create(:taxonomy) }
  let(:taxon) { create(:taxon, parent_id: taxonomy.root.id) }
  let(:taxon2) { create(:taxon, parent_id: taxonomy.root.id) }
  let(:product) { create(:product, taxons: [taxon]) }
  let(:notaxon_product) { create(:product) }
  let!(:product2) { create_list(:product, 5, taxons: [taxon]) }
  let!(:product3) { create(:product, taxons: [taxon2]) }

  before do
    visit potepan_product_path(product.id)
  end

  it "一覧ページへ戻るを選択するとカテゴリーページへ戻る" do
    expect(page).to have_link "一覧ページへ戻る"
    click_on "一覧ページへ戻る"
    expect(current_path).to eq potepan_category_path(taxon.id)
  end

  it "商品がカテゴリーに紐づいていない場合は一覧ページへ戻るが表示されない" do
    visit potepan_product_path(notaxon_product.id)
    expect(page).not_to have_content "一覧ページへ戻る"
  end

  it "関連商品が最大4点表示される" do
    expect(page).to have_selector ".productBox", count: 4
  end

  it "関連商品以外は表示されない" do
    expect(page).not_to have_content product3
  end

  it "関連商品を選択すると商品ページに推移する" do
    click_on product2.first.name
    expect(current_path).to eq potepan_product_path(product2.first.id)
  end
end
