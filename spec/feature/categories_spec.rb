RSpec.feature "Categories", type: :feature do
  let(:taxonomy) { create(:taxonomy) }
  let(:taxon) { create(:taxon, parent_id: taxonomy.root.id) }
  let!(:product) { create(:product, taxons: [taxon]) }
  let!(:product2) { create(:product) }

  before do
    visit potepan_category_path(taxon.id)
  end

  it "商品カテゴリーが表示され選択するとカテゴリーページに推移" do
    expect(page).to have_content taxonomy.name
    expect(page).to have_content taxon.name
    click_on taxon.name
    expect(current_path).to eq potepan_category_path(taxon.id)
  end

  it "カテゴリ一覧の商品が表示され選択すると商品ページへ推移" do
    expect(page).to have_content product.name
    expect(page).to have_content product.display_price
    click_on product.name
    expect(current_path).to eq potepan_product_path(product.id)
  end

  it "カテゴリー一覧にtaxon名が表示される" do
    within ".categories-list" do
      expect(page).to have_content taxon.name
    end
  end

  it "カテゴリーに紐づく商品名、値段がメインコンテンツに表示される" do
    within ".productBox" do
      expect(page).to have_content product.name
      expect(page).to have_content product.display_price
    end
  end

  it "カテゴリーに紐づいていない商品名、値段は表示されない" do
    within ".productBox" do
      expect(current_path).not_to have_content product2.name
      expect(current_path).not_to have_content product2.display_price
    end
  end
end
