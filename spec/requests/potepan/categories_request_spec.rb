RSpec.describe "Categories", type: :request do
  describe "GET potepan/categories" do
    let(:taxonomy) { create(:taxonomy) }
    let(:taxon) { create(:taxon) }
    let!(:product) { create(:product, taxons: [taxon]) }

    before do
      get potepan_category_path(taxon.id)
    end

    it "showページのリクエスト成功" do
      expect(response).to have_http_status(:success)
    end

    it "taxonomies名の表示成功" do
      expect(response.body).to include taxonomy.name
    end

    it "taxon名の表示成功" do
      expect(response.body).to include taxon.name
    end

    it "product名の表示成功" do
      expect(response.body).to include product.name
    end
  end
end
