RSpec.describe "index", type: :request do
  describe "GET /potepan/index" do
    it 'TOPページの表示成功' do
      get potepan_index_path
      expect(response).to have_http_status(200)
    end
  end
end
