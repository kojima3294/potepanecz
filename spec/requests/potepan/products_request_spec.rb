RSpec.describe "Products", type: :request do
  describe "GET /potepan/products" do
    let(:product) { create(:product) }

    before do
      get potepan_product_path(product.id)
    end

    it '詳細ページの表示成功' do
      expect(response).to have_http_status(200)
    end

    it '商品名の表示成功' do
      expect(response.body).to include product.name
    end

    it '商品金額の表示成功' do
      expect(response.body).to include product.display_price.to_s
    end

    it "商品の説明が正しく表示される" do
      expect(response.body).to include product.description
    end

    it "文言が正しく表示される" do
      expect(response.body).to include "カートへ入れる"
    end
  end
end
