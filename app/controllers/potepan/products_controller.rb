class Potepan::ProductsController < ApplicationController
  def show
    @product = Spree::Product.find(params[:id])
    @related_products = @product.related_products.limit(MAX_LIMIT_RELATED_PRODUCT).includes(master: [:default_price, :images])
  end
end
